# Latex Template Thesis

## ToDo

Bitte vor Bearbeitung die Parameter anpassen

```latex
%% !!!!!! BEARBEITEN !!!!! %%
%% ======================= %%
%% PARAMETER ZU DEFINIEREN %%
\newcommand{\parVorname}{dein_vorname} % -> \newcommand{\parVorname}{Lukas}
...
%% ======================= %%
```

## Kapitel
Neue Kapitel erstellen ohne Nummeriung

```latex
\chapter*{Überschrift}
% Sofern gewünscht, dass die Überschrift im Kopfbereich und im Inhaltsverzeichnis angezeigt wird.
%\addcontentsline{toc}{chapter}{Überschrift}
%\markboth{Überschrift}{Überschrift}
```

## Code Schnipsel

```bash
\begin{lstlisting}[language=Python, caption=Automatischer Download der Datensätze, label=code:automatischer_Download]
	# html
	r = requests.get(constant.DATASET_URL)
	content = r.content
	soup = BeautifulSoup(content, "html.parser")
		
	links = []
	# newst or certian date
	if(constant.DATASET_MONTHYEAR == 'newst'):
		city_table = soup.find(class_=constant.DATASET_CITY.lower())
		rows = (city_table.find_all('tr', class_=''))
		for tr in rows:
			cols = tr.findAll('td')
			if len(cols) > 0:
				links.append([cols[2].find('a').get('href'), cols[2].find('a').get_text(),
				cols[3].get_text(), datetimeDayMonthYearConverter(cols[0].get_text())]) 
	else:
		city_table = soup.find(class_=constant.DATASET_CITY.lower()).findAll('tr')[1:]
		filtered_city_table = list(filter(filter_records_by_date, city_table))
		query_date = filtered_city_table[0].td.text
		for row in filtered_city_table:
			cols = row.findChildren('td')
			val = [cols[2].find('a').get("href"), cols[2].find('a').get_text(),
			cols[3].text, datetimeDayMonthYearConverter(query_date)]
			links.append(val)
\end{lstlisting}
```

## Nummeriung

Entweder kleine Römische Zahlen (i, vi, etc.)

```latex
\pagenumbering{roman}
```

oder große Römische Zahlen (I, VI, etc.)
```latex
\pagenumbering{Roman}
```

## Abkürzungsverzeichnis

```latex
\acro{Kuerzel}[Kurzform]{Langform}
```
### Automatisches hinzufügen und sortieren

```bash
python sortAbbreviation.py "Kürzel" "Kurzform" "Langform"
```

## Silbentrennung

```bash
Staats\-ver\-trag.
```

## Referenzen

```bash
~\cite{Rodriguez.17.05.2018}
```

## Bilder

```bash
\begin{figure}[H]
	\centering
	\includegraphics[width=5cm]{img/used/sentiment_en.png}
	\caption{Prozentualer Anteil der Sentimentalität in den Bewertungen}
	\label{fig:sentiment_en}
\end{figure}

```


## Tabellen

Einfachster und unkompliziertesten Weg ist mit https://www.tablesgenerator.com/
```bash
\begin{table}[H]
	\caption{Superhost Klassifizierung}
	\label{tab:Superhost_Klassifizierung}
	\begin{tabularx}{\textwidth}{|l|X|}
		\hline
		\textbf{Kriterium} & \textbf{Beschreibung}
		\\ \hline
		\texttt{Gesamtbewertung} & Superhosts haben eine Gesamtbewertung von mindestens 4.8, basierend auf den Bewertungen ihrer Gäste von Airbnb im vergangenen Jahr.
		\\ \hline
		
		\texttt{Aufenthaltanzahl} & Superhosts haben im letzten Jahr mindestens 10 Aufenthalte ermöglicht oder an mindestens 100 Nächten bei mindestens drei Aufenthalten Gäste beherbergt. 
		\\ \hline	
		
		\texttt{Stonierungsquote} & Superhosts stornieren weniger als ein Prozent der Buchungen, besondere Umstände ausgenommen. Das bedeutet 0 Stornierungen bei Gastgebern mit weniger als 100 Buchungen im Jahr.
		\\ \hline
		
		\texttt{Antwortrate} & Superhosts antworten auf 90 Prozent ihrer neuen Nachrichten innerhalb von 24 Stunden. Wenn Gäste Fragen stellen, dann können sie sich darauf verlassen, dass sie schnell eine Antwort erhalten werden.
		\\ \hline		
	\end{tabularx}
\end{table}
```


## Kapitel und Abschnitte

```bash
\chapter{Kapitel}

\section{Überschrift 1}

\subsection{Überschrift 2}

\subsubsection{Überschrift 3}
```

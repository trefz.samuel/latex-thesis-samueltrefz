\boolfalse {citerequest}\boolfalse {citetracker}\boolfalse {pagetracker}\boolfalse {backtracker}\relax 
\babel@toc {ngerman}{}\relax 
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{Ehrenwörtliche Erklärung}{i}{chapter*.1}%
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{Zusammenfassung}{ii}{chapter*.2}%
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{Abbildungsverzeichnis}{vii}{chapter*.4}%
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{Tabellenverzeichnis}{viii}{chapter*.5}%
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{Abkürzungsverzeichnis}{ix}{chapter*.6}%
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{Codeverzeichnis}{x}{chapter*.7}%
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {1}Einleitung}{1}{chapter.1}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {1.1}Hintergrund der Arbeit}{1}{section.1.1}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {1.2}Zielsetzung der Arbeit}{1}{section.1.2}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {1.3}Aufbau}{1}{section.1.3}%
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {2}Grundlagen}{2}{chapter.2}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.1}Mensch-Maschine-Kommunikation}{2}{section.2.1}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.1.1}User Interface Design}{2}{subsection.2.1.1}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.1.2}User Experience Testing}{3}{subsection.2.1.2}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.2}Standardsoftware}{3}{section.2.2}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.3}Individual Software}{3}{section.2.3}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.4}Microservices}{4}{section.2.4}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.5}Grundlagen von Webanwendungen}{4}{section.2.5}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.5.1}Client-Server Architektur}{4}{subsection.2.5.1}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.5.2}Aufbau einer Webapplikation}{5}{subsection.2.5.2}%
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {2.5.2.1}Frontend}{5}{subsubsection.2.5.2.1}%
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {2.5.2.2}Backend}{6}{subsubsection.2.5.2.2}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.5.3}Kommunikationsaustausch zwischen Webapplikationen}{6}{subsection.2.5.3}%
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {2.5.3.1}Representational State Transfer}{6}{subsubsection.2.5.3.1}%
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {2.5.3.2}GraphQL}{6}{subsubsection.2.5.3.2}%
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {2.5.3.3}Gegenüberstellung REST und GraphQL}{7}{subsubsection.2.5.3.3}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.6}Prototyping}{7}{section.2.6}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.6.1}Arten des Prototypings}{7}{subsection.2.6.1}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.6.2}Muster von Prototypen}{8}{subsection.2.6.2}%
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {2.6.2.1}Horizontaler Prototyp}{8}{subsubsection.2.6.2.1}%
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {2.6.2.2}Vertikaler Prototyp}{8}{subsubsection.2.6.2.2}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.7}Anforderungsanalyse in der Softwareentwicklung}{9}{section.2.7}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.8}Benachrichtigungen}{9}{section.2.8}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.8.1}Ereignisgesteuerte Architektur}{10}{subsection.2.8.1}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.8.2}Publisher- und Subscriber-Systeme}{10}{subsection.2.8.2}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.8.3}Benachrichtigungsarten}{12}{subsection.2.8.3}%
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {3}Methodische Grundlagen}{13}{chapter.3}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3.1}Vorgehensmodelle in der Softwareentwicklung}{13}{section.3.1}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3.2}Design Thinking}{13}{section.3.2}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.2.1}Erforschung des Problemraums}{13}{subsection.3.2.1}%
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {3.2.1.1}Phase I: Verstehen}{13}{subsubsection.3.2.1.1}%
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {3.2.1.2}Phase II: Beobachten und Erforschen}{13}{subsubsection.3.2.1.2}%
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {3.2.1.3}Phase III: Synthese}{13}{subsubsection.3.2.1.3}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.2.2}Erforschung des Lösungsraums}{13}{subsection.3.2.2}%
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {3.2.2.1}Phase IV: Ideenfindung}{14}{subsubsection.3.2.2.1}%
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {3.2.2.2}Phase V: Prototyp Entwicklung}{14}{subsubsection.3.2.2.2}%
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {3.2.2.3}Phase VI: Testen}{14}{subsubsection.3.2.2.3}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.2.3}Möglichkeiten und Grenzen des Design Thinkings}{14}{subsection.3.2.3}%
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {4}Ausgangsbasis im Unternehmen}{15}{chapter.4}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4.1}Informationen zur AEB SE}{15}{section.4.1}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.1.1}Produktportfolio der AEB über die Jahre}{15}{subsection.4.1.1}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4.2}Vertrieb an die Kunden}{15}{section.4.2}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4.3}Teamgestaltung}{15}{section.4.3}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4.4}Einordnung der Masterarbeit}{17}{section.4.4}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4.5}Software-Architektur der Softwareprodukte der AEB}{17}{section.4.5}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4.6}Stakeholder des Notification-Services}{19}{section.4.6}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4.7}Publisher und Subscriber im AEB-Kontext}{20}{section.4.7}%
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {5}Erforschung des Problemraums}{21}{chapter.5}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {5.1}Phase 1: Verstehen}{21}{section.5.1}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {5.1.1}Soll-Ist-Analyse}{23}{subsection.5.1.1}%
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {5.1.1.1}Ist-Zustand im Benachrichtigungsmanagement der AEB}{23}{subsubsection.5.1.1.1}%
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {5.1.1.2}Publisher-/Subscriber-Übersicht der AEB SE}{25}{subsubsection.5.1.1.2}%
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {5.1.1.3}Soll-Zustand für einen Benachrichtigungsdienst}{25}{subsubsection.5.1.1.3}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {5.1.2}Persona}{26}{subsection.5.1.2}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {5.1.3}Use-Cases des Benachrichtigungsdienstes}{26}{subsection.5.1.3}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {5.2}Erste graphische Oberfläche}{26}{section.5.2}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {5.2.1}Oberflächenkomponenten}{27}{subsection.5.2.1}%
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {5.2.1.1}Youtube}{27}{subsubsection.5.2.1.1}%
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {5.2.1.2}Facebook}{27}{subsubsection.5.2.1.2}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {5.2.2}AEB-Oberflächenkomponenten}{28}{subsection.5.2.2}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {5.2.3}Erstellte Oberflächenkomponenten}{29}{subsection.5.2.3}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {5.2.4}Oberfläche des Benachrichtigungsdienstes für den Nutzer}{30}{subsection.5.2.4}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {5.2.5}Oberfläche der Benachrichtigungseinstellung}{30}{subsection.5.2.5}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {5.3}Phase 2: Beobachten und Erforschen}{30}{section.5.3}%
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {6}Erforschung des Lösungsraums}{31}{chapter.6}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {6.1}Prototyp}{31}{section.6.1}%
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {7}Zusammenfassung und Ausblick}{32}{chapter.7}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {7.1}Wesentliche Ergebnisse}{32}{section.7.1}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {7.2}Weiterführende Arbeiten}{32}{section.7.2}%

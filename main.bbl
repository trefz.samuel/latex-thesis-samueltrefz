% $ biblatex auxiliary file $
% $ biblatex bbl format version 3.1 $
% Do not modify the above lines!
%
% This is an auxiliary file used by the 'biblatex' package.
% This file may safely be deleted. It will be recreated as
% required.
%
\begingroup
\makeatletter
\@ifundefined{ver@biblatex.sty}
  {\@latex@error
     {Missing 'biblatex' package}
     {The bibliography requires the 'biblatex' package.}
      \aftergroup\endinput}
  {}
\endgroup

\datalist[entry]{none/global//global/global}
  \entry{Heinecke.2012}{book}{}
    \name{author}{1}{}{%
      {{hash=HAM}{%
         family={Heinecke},
         familyi={H\bibinitperiod},
         given={Andreas\bibnamedelima M.},
         giveni={A\bibinitperiod\bibinitdelim M\bibinitperiod},
      }}%
    }
    \list{publisher}{1}{%
      {{Springer Berlin Heidelberg}}%
    }
    \strng{namehash}{HAM1}
    \strng{fullhash}{HAM1}
    \field{labelnamesource}{author}
    \field{labeltitlesource}{title}
    \field{labelyear}{2012}
    \field{labeldatesource}{}
    \verb{doi}
    \verb 10.1007/978-3-642-13507-1
    \endverb
    \field{isbn}{978-3-642-13506-4}
    \field{pagetotal}{391}
    \field{title}{Mensch-Computer-Interaktion}
    \list{location}{1}{%
      {Berlin, Heidelberg}%
    }
    \verb{file}
    \verb Heinecke2012{\_}Book{\_}Mensch-Computer-Interaktion:Attachments/Heine
    \verb cke2012{\_}Book{\_}Mensch-Computer-Interaktion.pdf:application/pdf
    \endverb
    \field{year}{2012}
  \endentry

  \entry{Tremp.2021}{book}{}
    \name{author}{1}{}{%
      {{hash=TH}{%
         family={Tremp},
         familyi={T\bibinitperiod},
         given={Hansruedi},
         giveni={H\bibinitperiod},
      }}%
    }
    \list{publisher}{1}{%
      {{Springer Fachmedien Wiesbaden}}%
    }
    \strng{namehash}{TH1}
    \strng{fullhash}{TH1}
    \field{labelnamesource}{author}
    \field{labeltitlesource}{title}
    \field{labelyear}{2021}
    \field{labeldatesource}{}
    \verb{doi}
    \verb 10.1007/978-3-658-33179-5
    \endverb
    \field{isbn}{978-3-658-33178-8}
    \field{pagetotal}{193}
    \field{title}{Architekturen Verteilter Softwaresysteme}
    \list{location}{1}{%
      {Wiesbaden}%
    }
    \verb{file}
    \verb Tremp2021{\_}Book{\_}ArchitekturenVerteilterSoftwar:Attachments/Tremp
    \verb 2021{\_}Book{\_}ArchitekturenVerteilterSoftwar.pdf:application/pdf
    \endverb
    \field{year}{2021}
  \endentry

  \entry{Hegner.2003}{article}{}
    \name{author}{1}{}{%
      {{hash=HM}{%
         family={Hegner},
         familyi={H\bibinitperiod},
         given={Marcus},
         giveni={M\bibinitperiod},
      }}%
    }
    \keyw{Mai 2003}
    \strng{namehash}{HM1}
    \strng{fullhash}{HM1}
    \field{labelnamesource}{author}
    \field{labeltitlesource}{title}
    \field{labelyear}{2003}
    \field{labeldatesource}{}
    \field{abstract}{%
    IZ-Arbeitsbericht Nr. 29%
    }
    \field{pagetotal}{98}
    \field{title}{Methoden zur Evaluation von Software}
    \verb{file}
    \verb Methoden zur Evaluation von Software:Attachments/Methoden zur Evaluat
    \verb ion von Software.pdf:application/pdf
    \endverb
    \field{year}{2003}
  \endentry

  \entry{mindworksGmbHSoftwareentwicklung.2017}{online}{}
    \name{author}{1}{}{%
      {{hash=m}{%
         family={{mindworks GmbH}},
         familyi={m\bibinitperiod},
      }}%
    }
    \strng{namehash}{m1}
    \strng{fullhash}{m1}
    \field{labelnamesource}{author}
    \field{labeltitlesource}{title}
    \field{labelyear}{2017}
    \field{labeldatesource}{}
    \field{abstract}{%
    Individualsoftware vs. Standardsoftware. Im Ergebnis entscheiden sich die
  meisten Unternehmen f{\"u}r Individuall{\"o}sungen bei Ihren
  Gesch{\"a}ftsprozessen%
    }
    \field{title}{Individualsoftware vs. Standardsoftware im Vergleich}
    \verb{url}
    \verb https://www.mindworks.de/individualsoftware-vs-standardsoftware-vergl
    \verb eich/
    \endverb
    \field{year}{2017}
    \field{urlday}{05}
    \field{urlmonth}{05}
    \field{urlyear}{2022}
  \endentry

  \entry{V.2015}{article}{}
    \name{author}{1}{}{%
      {{hash=VBe}{%
         family={V},
         familyi={V},
         given={Bitkom\bibnamedelima e.},
         giveni={B\bibinitperiod\bibinitdelim e\bibinitperiod},
      }}%
    }
    \strng{namehash}{VBe1}
    \strng{fullhash}{VBe1}
    \field{labelnamesource}{author}
    \field{labeltitlesource}{title}
    \field{labelyear}{2015}
    \field{labeldatesource}{}
    \field{pages}{4\bibrangedash 33}
    \field{pagetotal}{36}
    \field{pagination}{page}
    \field{subtitle}{Leitfade Webentwicklung}
    \field{title}{Entwicklung erfolgreicher Webanwendungen}
    \verb{url}
    \verb https://www.bitkom.org/sites/default/files/file/import/LF-Webanwendun
    \verb gen-150910-1.pdf
    \endverb
    \verb{file}
    \verb LF-Webanwendungen-150910-1:Attachments/LF-Webanwendungen-150910-1.pdf
    \verb :application/pdf;V 2015 - Entwicklung erfolgreicher Webanwendungen:At
    \verb tachments/V 2015 - Entwicklung erfolgreicher Webanwendungen.pdf:appli
    \verb cation/pdf
    \endverb
    \field{year}{2015}
    \field{urlday}{08}
    \field{urlmonth}{04}
    \field{urlyear}{2022}
  \endentry

  \entry{kreITiv.2018}{online}{}
    \name{author}{1}{}{%
      {{hash=FD}{%
         family={Faustmann},
         familyi={F\bibinitperiod},
         given={Dirk},
         giveni={D\bibinitperiod},
      }}%
    }
    \strng{namehash}{FD1}
    \strng{fullhash}{FD1}
    \field{labelnamesource}{author}
    \field{labeltitlesource}{title}
    \field{labelyear}{2018}
    \field{labeldatesource}{}
    \field{abstract}{%
    Wir erl{\"a}utern die Urspr{\"u}nge und Unterschiede des Prototypings in
  der Softwareentwicklung: Exploratives, experimentelles und evolution{\"a}res
  Prototyping.%
    }
    \field{title}{3 Arten von Prototyping in der Softwareentwicklung}
    \verb{url}
    \verb https://www.kreitiv.de/arten-prototyping-softwareentwicklung/
    \endverb
    \field{year}{2018}
    \field{urlday}{12}
    \field{urlmonth}{04}
    \field{urlyear}{2022}
  \endentry

  \entry{.12.04.2022}{online}{}
    \name{author}{1}{}{%
      {{hash=KM}{%
         family={Kuhrmann},
         familyi={K\bibinitperiod},
         given={Marco},
         giveni={M\bibinitperiod},
      }}%
    }
    \strng{namehash}{KM1}
    \strng{fullhash}{KM1}
    \field{labelnamesource}{author}
    \field{labeltitlesource}{title}
    \field{labelyear}{2012}
    \field{labeldatesource}{}
    \field{title}{Prototyping --- Enzyklopaedie der Wirtschaftsinformatik}
    \verb{url}
    \verb https://www.enzyklopaedie-der-wirtschaftsinformatik.de/lexikon/is-man
    \verb agement/Systementwicklung/Vorgehensmodell/Prototyping/index.html
    \endverb
    \field{year}{2012}
    \field{urlday}{12}
    \field{urlmonth}{04}
    \field{urlyear}{2022}
  \endentry

  \entry{Meyer.2018}{book}{}
    \name{author}{1}{}{%
      {{hash=MA}{%
         family={Meyer},
         familyi={M\bibinitperiod},
         given={Albin},
         giveni={A\bibinitperiod},
      }}%
    }
    \list{language}{1}{%
      {ger}%
    }
    \list{organization}{1}{%
      {{De Gruyter Oldenbourg}}%
    }
    \list{publisher}{1}{%
      {{De Gruyter Oldenbourg}}%
    }
    \strng{namehash}{MA1}
    \strng{fullhash}{MA1}
    \field{labelnamesource}{author}
    \field{labeltitlesource}{title}
    \field{labelyear}{2018}
    \field{labeldatesource}{}
    \verb{doi}
    \verb 10.1515/9783110578379-202
    \endverb
    \field{isbn}{978-3-11-057580-4}
    \field{note}{Meyer, Albin (VerfasserIn)}
    \field{pagetotal}{271}
    \field{series}{Praxishandbuch}
    \field{subtitle}{Ein Kompass f{\"u}r die Praxis}
    \field{title}{Softwareentwicklung}
    \list{location}{3}{%
      {Berlin}%
      {M{\"u}nchen}%
      {Boston}%
    }
    \verb{file}
    \verb [9783110578379 - Softwareentwicklung] Softwareentwicklung:Attachments
    \verb /[9783110578379 - Softwareentwicklung] Softwareentwicklung.pdf:applic
    \verb ation/pdf
    \endverb
    \field{year}{2018}
  \endentry

  \entry{AmazonWebServicesInc..29.04.2022}{online}{}
    \name{author}{1}{}{%
      {{hash=A}{%
         family={AWS},
         familyi={A\bibinitperiod},
      }}%
    }
    \strng{namehash}{A1}
    \strng{fullhash}{A1}
    \field{labelnamesource}{author}
    \field{labeltitlesource}{title}
    \field{labelyear}{2022}
    \field{labeldatesource}{}
    \field{abstract}{%
    An event-driven architecture uses events to trigger and communicate between
  services. Learn more about its benefits, use cases, and getting started.%
    }
    \field{subtitle}{Decoupled systems that run in response to events}
    \field{title}{What is an Event-Driven Architecture?}
    \verb{url}
    \verb https://aws.amazon.com/de/event-driven-architecture/
    \endverb
    \field{year}{2022}
    \field{urlday}{04}
    \field{urlmonth}{05}
    \field{urlyear}{2022}
  \endentry

  \entry{ZeidlerAndreas.2004}{thesis}{}
    \name{author}{1}{}{%
      {{hash=ZA}{%
         family={Zeidler},
         familyi={Z\bibinitperiod},
         given={Andreas},
         giveni={A\bibinitperiod},
      }}%
    }
    \strng{namehash}{ZA1}
    \strng{fullhash}{ZA1}
    \field{labelnamesource}{author}
    \field{labeltitlesource}{title}
    \field{labelyear}{2004}
    \field{labeldatesource}{}
    \field{pagetotal}{211}
    \field{titleaddon}{Informatik}
    \field{title}{A Distributed Publish/Subscribe Notification Service for
  Pervasive Environments}
    \list{institution}{1}{%
      {{Technische Universit{\"a}t Darmstadt}}%
    }
    \verb{file}
    \verb Dissertation{\_}Notification:Attachments/Dissertation{\_}Notification
    \verb .pdf:application/pdf
    \endverb
    \field{year}{2004}
  \endentry

  \entry{ElGazzar.2010}{article}{}
    \name{author}{3}{}{%
      {{hash=EGRF}{%
         family={El-Gazzar},
         familyi={E\bibinithyphendelim G\bibinitperiod},
         given={Rania\bibnamedelima Fahim},
         giveni={R\bibinitperiod\bibinitdelim F\bibinitperiod},
      }}%
      {{hash=BO}{%
         family={Badawy},
         familyi={B\bibinitperiod},
         given={Osama},
         giveni={O\bibinitperiod},
      }}%
      {{hash=KM}{%
         family={Kholief},
         familyi={K\bibinitperiod},
         given={Mohamed},
         giveni={M\bibinitperiod},
      }}%
    }
    \strng{namehash}{EGRF+1}
    \strng{fullhash}{EGRFBOKM1}
    \field{labelnamesource}{author}
    \field{labeltitlesource}{title}
    \field{labelyear}{2010}
    \field{labeldatesource}{}
    \verb{doi}
    \verb 10.3991/ijim.v4i4.1427
    \endverb
    \field{number}{4}
    \field{pages}{25}
    \field{pagetotal}{10}
    \field{pagination}{page}
    \field{shortjournal}{Int. J. Interact. Mob. Technol.}
    \field{title}{Agent-Based Mobile Event Notification System}
    \field{volume}{4}
    \verb{file}
    \verb SubscriberModell:Attachments/SubscriberModell.pdf:application/pdf
    \endverb
    \field{journaltitle}{International Journal of Interactive Mobile
  Technologies (iJIM)}
    \field{year}{2010}
  \endentry

  \entry{Schallmo.2020}{book}{}
    \name{author}{2}{}{%
      {{hash=SDR}{%
         family={Schallmo},
         familyi={S\bibinitperiod},
         given={Daniel\bibnamedelima R.A.},
         giveni={D\bibinitperiod\bibinitdelim R\bibinitperiod},
      }}%
      {{hash=LK}{%
         family={Lang},
         familyi={L\bibinitperiod},
         given={Klaus},
         giveni={K\bibinitperiod},
      }}%
    }
    \list{publisher}{1}{%
      {{Springer Fachmedien Wiesbaden}}%
    }
    \strng{namehash}{SDRLK1}
    \strng{fullhash}{SDRLK1}
    \field{labelnamesource}{author}
    \field{labeltitlesource}{title}
    \field{labelyear}{2020}
    \field{labeldatesource}{}
    \verb{doi}
    \verb 10.1007/978-3-658-28325-4
    \endverb
    \field{isbn}{978-3-658-28324-7}
    \field{pagetotal}{184}
    \field{title}{Design Thinking erfolgreich anwenden}
    \list{location}{1}{%
      {Wiesbaden}%
    }
    \verb{file}
    \verb Schallmo-Lang2020{\_}Book{\_}DesignThinkingErfolgreichAnwen:Attachmen
    \verb ts/Schallmo-Lang2020{\_}Book{\_}DesignThinkingErfolgreichAnwen.pdf:ap
    \verb plication/pdf
    \endverb
    \field{year}{2020}
  \endentry

  \entry{BMI.04.05.2022}{online}{}
    \name{author}{1}{}{%
      {{hash=B}{%
         family={BMI},
         familyi={B\bibinitperiod},
      }}%
    }
    \name{editor}{1}{}{%
      {{hash=B}{%
         family={{Bundesministerium des Innern und f{\"u}r Heimat}},
         familyi={B\bibinitperiod},
      }}%
    }
    \strng{namehash}{B1}
    \strng{fullhash}{B1}
    \field{labelnamesource}{author}
    \field{labeltitlesource}{title}
    \field{labelyear}{04.05.2022}
    \field{labeldatesource}{}
    \field{abstract}{%
    Soll-Konzeption%
    }
    \field{title}{Organisationshandbuch - Soll-Konzeption}
    \verb{url}
    \verb https://www.orghandbuch.de/OHB/DE/Organisationshandbuch/2_Vorgehensmo
    \verb dell/23_Hauptuntersuchung/233_SollKonzeption/sollkonzeption-node.html
    \endverb
    \field{year}{04.05.2022}
    \field{urlday}{04}
    \field{urlmonth}{05}
    \field{urlyear}{2022}
  \endentry

  \entry{Leimeister.2020}{book}{}
    \name{author}{1}{}{%
      {{hash=LJM}{%
         family={Leimeister},
         familyi={L\bibinitperiod},
         given={Jan\bibnamedelima Marco},
         giveni={J\bibinitperiod\bibinitdelim M\bibinitperiod},
      }}%
    }
    \list{publisher}{1}{%
      {{Springer Berlin Heidelberg}}%
    }
    \strng{namehash}{LJM1}
    \strng{fullhash}{LJM1}
    \field{labelnamesource}{author}
    \field{labeltitlesource}{title}
    \field{labelyear}{2020}
    \field{labeldatesource}{}
    \verb{doi}
    \verb 10.1007/978-3-662-59858-0
    \endverb
    \field{isbn}{978-3-662-59857-3}
    \field{pagetotal}{471}
    \field{title}{Dienstleistungsengineering und -management}
    \list{location}{1}{%
      {Berlin, Heidelberg}%
    }
    \verb{file}
    \verb 2020{\_}Book{\_}DienstleistungsengineeringUnd:Attachments/2020{\_}Boo
    \verb k{\_}DienstleistungsengineeringUnd.pdf:application/pdf
    \endverb
    \field{year}{2020}
  \endentry
\enddatalist
\endinput
